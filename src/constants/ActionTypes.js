export const GET_ALL_TODO = 'GET_ALL_TODO';
export const DELETE_TODO = 'DELETE_TODO';
export const ADD_TODO = 'ADD_TODO';
export const UPDATE_TODO = 'UPDATE_TODO';
export const GET_COMPLETE_TODO = 'GET_COMPLETE_TODO';
export const SEARCH_TODO = 'SEARCH_TODO';
export const CATCH_ERROR = 'CATCH_ERROR';