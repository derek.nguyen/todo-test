import * as Types from './../constants/ActionTypes';
import api from './../utils/api';


export const catchError = (error) => {
    return {
        type: Types.CATCH_ERROR,
        error: error
    }
}

export const getAllTodoRequest = () => {
    return (distpatch) => {
        return api(
            'todos',
            'GET',
            null,
            null
        ).then(res => {
            console.log(res);
            distpatch(getAllTodo(res));
        }).catch(err => {
            console.log(err);
            distpatch(catchError(err.toString()));
        });
    }
}
export const getAllTodo = (getAllResult) => {
    return {
        type: Types.GET_ALL_TODO,
        getAllResult: getAllResult
    }
}


export const deleteTodoRequest = (id) => {
    return (distpatch) => {
        return api(
            `todos/${id}`,
            'DELETE',
            null,
            null
        ).then(res => {
            console.log(res);
            distpatch(deleteTodo({
                id: id,
                res: res
            }));
        }).catch(err => {
            console.log(err);
            distpatch(catchError(err.toString()));
        });
    }
}
export const deleteTodo = (deleteResult) => {
    return {
        type: Types.DELETE_TODO,
        deleteResult: deleteResult
    }
}

export const addTodoRequest = (todo) => {
    return (distpatch) => {
        return api(
            'todos',
            'POST',
            todo,
            null
        ).then(res => {
            console.log('add',res);
            distpatch(addTodo(res));
        }).catch(err => {
            console.log(err);
            distpatch(catchError(err.toString()));
        });
    }
}
export const addTodo = (addResult) => {
    return {
        type: Types.ADD_TODO,
        addResult: addResult
    }
}

export const updateTodoRequest = (id, newTodo) => {
    return (distpatch) => {
        return api(
            `todos/${id}`,
            'PUT',
            newTodo,
            null
        ).then(res => {
            console.log('update',res);
            distpatch(updateTodo(res));
        }).catch(err => {
            console.log(err);
            distpatch(catchError(err.toString()));
        });
    }
}
export const updateTodo = (updateResult) => {
    return {
        type: Types.UPDATE_TODO,
        updateResult: updateResult
    }
}

export const getCompleteTodoRequest = () => {
    return (distpatch) => {
        return api(
            `todos?status=true`,
            'GET',
            null,
            null
        ).then(res => {
            distpatch(getCompleteTodo(res));
        }).catch(err => {
            console.log(err);
            distpatch(catchError(err.toString()));
        });
    }
}
export const getCompleteTodo = (getCompleteTodoResult) => {
    return {
        type: Types.GET_COMPLETE_TODO,
        getCompleteTodoResult: getCompleteTodoResult
    }
}

export const searchTodoRequest = (title) => {
    return (distpatch) => {
        return api(
            `todos?title_like=${title}`,
            'GET',
            null,
            null
        ).then(res => {
            distpatch(searchTodo(res));
        }).catch(err => {
            console.log(err);
            distpatch(catchError(err.toString()));
        });
    }
}
export const searchTodo = (searchResult) => {
    return {
        type: Types.SEARCH_TODO,
        searchResult: searchResult
    }
}