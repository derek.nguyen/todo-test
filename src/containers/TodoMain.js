import React, {Component} from 'react';
import {
    getAllTodoRequest,
    deleteTodoRequest,
    addTodoRequest,
    updateTodoRequest,
    getCompleteTodoRequest,
    searchTodoRequest
} from '../actions/index';
import {connect} from 'react-redux';
import List from "../components/List";
import TodoForm from "../components/TodoForm";
import TodoAction from "../components/TodoAction";

class TodoMain extends Component{
    constructor(props){
        super(props);
        this.state = {
            isSearch: false,
        }
    }
    componentDidMount() {
        this.props.getAllTodoRequest();
    }
    onDeleteTodo = (id) => this.props.deleteTodoRequest(id);
    onStatusChange = (id, newTodo) => this.props.updateTodoRequest(id, newTodo);

    onSubmit = (title) => {
        if(!this.state.isSearch){
            this.props.adddTodoRequest({
                title: title,
                status: false
            });
        }
    }
    onGetComplete = () => this.props.getCompleteTodoRequest();
    onGetAll = () => this.props.getAllTodoRequest();
    onSearch = () => this.setState({ isSearch: true });
    onAdd = () => this.setState({ isSearch: false });
    onChange = (title) => {
        let {isSearch} = this.state;
        if(isSearch){
            this.props.searchTodoRequest(title);
        }
    }
    onUpdateTodo = (newTodo) => {
        this.props.updateTodoRequest(newTodo.id, newTodo);
    }

    render(){
        return(
            <div className="row justify-content-center">
                <div className="col-md-8 col-lg-6">
                    <div className="card mt-5">
                        <div className="card-header text-uppercase text-center">
                            <strong>Things todo</strong>
                        </div>
                        <div className="card-body">
                            <div className="form">
                                <TodoForm
                                    onSubmit={this.onSubmit}
                                    isSearch={this.state.isSearch}
                                    onChange={this.onChange}
                                />
                            </div>
                            <div className="list">
                                <List
                                    todos={this.props.todos.data}
                                    onDeleteTodo={this.onDeleteTodo}
                                    onStatusChange={this.onStatusChange}
                                    onUpdateTodo={this.onUpdateTodo}
                                />
                            </div>
                        </div>
                        <div className="card-footer">
                            <TodoAction
                                total={this.props.todos.data.length}
                                onGetComplete={this.onGetComplete}
                                onGetAll={this.onGetAll}
                                onSearch={this.onSearch}
                                onAdd={this.onAdd}
                            />
                            {
                                (this.props.todos.error)
                                ?
                                <div className="text-center text-danger mt-4">
                                    {this.props.todos.error}
                                </div>
                                :
                                ''
                            }

                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
const mapStateToProps = (state) => {
    return {
        todos: state.todos
    }
}

const mapDispatchToProps = (dispatch) => {
    return  {
        getAllTodoRequest: () => {
            dispatch(getAllTodoRequest());
        },
        deleteTodoRequest: (id) => {
            dispatch(deleteTodoRequest(id));
        },
        adddTodoRequest: (todo) => {
            dispatch(addTodoRequest(todo));
        },
        updateTodoRequest: (id, newTodo) => {
            dispatch(updateTodoRequest(id, newTodo));
        },
        getCompleteTodoRequest: () => {
            dispatch(getCompleteTodoRequest());
        },
        searchTodoRequest: (title) => {
            dispatch(searchTodoRequest(title));
        }
    }
}
export default (connect(mapStateToProps, mapDispatchToProps))(TodoMain);