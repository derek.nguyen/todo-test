import * as Types from './../constants/ActionTypes';
const initialState = {
    error: '',
    message: '',
    status: 0,
    data: []
};

const todos = (state = initialState, action) => {
    switch(action.type){
        case Types.CATCH_ERROR :
            return {
                ...state,
                error: action.error
            };
        case Types.GET_ALL_TODO :
            return {
                ...state,
                error: '',
                message: action.getAllResult.statusText,
                status: action.getAllResult.status,
                data: action.getAllResult.data
            };
        case Types.DELETE_TODO :
            let todos = state.data;
            let todosAfterDeleted = todos.filter((todo) => {
                return todo.id !== action.deleteResult.id
            });

            return {
                ...state,
                error: '',
                message: action.deleteResult.res.statusText,
                status: action.deleteResult.res.status,
                data: todosAfterDeleted
            };
        case Types.ADD_TODO :
            const newTodos = [...state.data, ...[action.addResult.data]];

            return {
                ...state,
                error: '',
                message: action.addResult.statusText,
                status: action.addResult.status,
                data: newTodos
            };
        case Types.UPDATE_TODO :
            let listTodos = state.data;
            const index = listTodos.findIndex(item => {
                    return item.id === action.updateResult.data.id
                }
            );
            if(index >= 0){
                listTodos[index] = action.updateResult.data
            }
            return {
                ...state,
                error: '',
                message: action.updateResult.statusText,
                status: action.updateResult.status,
                data: listTodos
            };
        case Types.GET_COMPLETE_TODO :
            return {
                ...state,
                error: '',
                message: action.getCompleteTodoResult.statusText,
                status: action.getCompleteTodoResult.status,
                data: action.getCompleteTodoResult.data
            };
        case Types.SEARCH_TODO :
            return {
                ...state,
                error: '',
                message: action.searchResult.statusText,
                status: action.searchResult.status,
                data: action.searchResult.data
            };
        default:
            return state;
    }
}
export default todos;