import React from 'react';
import './assets/scss/style.scss';
import TodoMain from "./containers/TodoMain";
function App() {
    return (
        <div className="App">
            <div className="container">
                <TodoMain />
            </div>
        </div>
    );
}

export default App;
