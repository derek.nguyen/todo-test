import React, {Component} from 'react';

class TodoForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            title: ''
        }
    }
    onChange = (e) => {
        var target = e.target;
        var value = target.value;
        this.setState({
            title: value
        });
        this.props.onChange(value);
    }
    onSubmit = (e) => {
        e.preventDefault();
        let {title} = this.state;
        this.props.onSubmit(title);
        this.setState({
            title: ''
        });
    }

    render() {
        return (
            <form onSubmit={this.onSubmit}>
                <div className="form-group">
                    <input
                        type="text"
                        name="title"
                        onChange={this.onChange}
                        value={this.state.title}
                        className="form-control"
                        placeholder={this.props.isSearch ? 'Search' : 'Add'}
                    />
                </div>
            </form>
        )
    }
}

export default TodoForm;