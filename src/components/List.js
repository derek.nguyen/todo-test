import React, {Component} from 'react';
import { Modal, Button } from 'react-bootstrap';
class List extends Component{
    constructor(props){
        super(props);
        this.state = {
            show: false,
            newTodo: {
                id: -1,
                title: '',
                status: false
            }
        };
    }
    onTitleUpdateChange = (e) => {
        let target = e.target;
        let value = target.value;

        this.setState({
            newTodo: {
                id: this.state.newTodo.id,
                title: value,
                status: this.state.newTodo.status
            }
        });

    }
    onCloseModal = () => {
        this.setState({
            show: false
        });
    }
    onShowUpdateForm = (item) => {
        this.setState({
            show: true
        });
        this.setState({
            newTodo: item
        });
    }

    onStatusChange = (id, newTodo) => this.props.onStatusChange(id,newTodo);
    onDeleteTodo = (id) => this.props.onDeleteTodo(id);
    onUpdateTodo = (newTodo) => {
        this.props.onUpdateTodo(newTodo);
        this.setState({
            show: false
        });
    }
    render(){
        let {todos} = this.props;
        const list = todos.map((item, index) => {
            return (
                <li key={index}>
                    <div className="d-flex">
                        <span>
                            <input
                                id={item.id}
                                type="checkbox"
                                defaultChecked={item.status}
                                onChange={
                                    () => this.onStatusChange(
                                        item.id,
                                        {
                                            title: item.title,
                                            status:!item.status
                                        }
                                    )
                                }
                            />
                            {
                                (!item.status)
                                ?
                                <label htmlFor={item.id}>{item.title}</label>
                                :
                                <label htmlFor={item.id}><del>{item.title}</del></label>
                            }

                        </span>
                        <span className="ml-auto">
                            <button
                                onClick={() => this.onShowUpdateForm(item) }
                                className="btn text-primary mr-3"
                            >
                                Edit
                            </button>
                            <button  onClick={() => this.onDeleteTodo(item.id) }>
                                &times;
                            </button>
                        </span>
                    </div>
                </li>
            )
        });
        return(
            <div>
                <ul className="list-unstyled">
                    {list}
                </ul>
                <Modal show={this.state.show} onHide={this.onCloseModal}>
                    <Modal.Header closeButton>
                        <Modal.Title>Update todo</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <div className="form-group">
                            <input
                                className="form-control"
                                type="text"
                                value={this.state.newTodo.title}
                                onChange={this.onTitleUpdateChange}
                                name="titleUpdate"
                            />
                        </div>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button
                            variant="secondary"
                            onClick={this.onCloseModal}
                        >
                            Close
                        </Button>
                        <Button
                            variant="primary"
                            onClick={() => this.onUpdateTodo(this.state.newTodo)}
                        >
                            Update
                        </Button>
                    </Modal.Footer>
                </Modal>
            </div>
        )
    }
}
export default List;