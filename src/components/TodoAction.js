import React, {Component} from 'react';
class TodoAction extends Component{
    onGetComplete = () => this.props.onGetComplete();
    onGetAll = () => this.props.onGetAll();
    onSearch = () => this.props.onSearch();
    onAdd = () => this.props.onAdd();
    render(){
        return(
            <div className="actions">
                <div className="row">
                    <div className="col-md-6">
                        <button
                            className="btn btn-secondary mr-3"
                            type="button"
                            onClick={this.onAdd}
                        >
                            Add
                        </button>
                        <button
                            className="btn btn-success mr-3"
                            type="button"
                            onClick={this.onSearch}
                        >
                            Search
                        </button>
                        <span className="total">
                            {this.props.total} items
                        </span>
                    </div>
                    <div className="col-md-6 text-right">
                        <button
                            className="btn btn-secondary mr-3"
                            type="button"
                            onClick={this.onGetAll}
                        >
                            All
                        </button>
                        <button
                            className="btn btn-primary"
                            type="button"
                            onClick={this.onGetComplete}
                        >
                            Complete
                        </button>
                    </div>
                </div>
            </div>
        )
    }
}
export default TodoAction;